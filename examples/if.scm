(if (< 1 2) 4 5)
(if (> 1 2) (* 2 2) (/ 5 2))
(if (= 1 2) (* 2 2) (/ 5 2))
(if (\= 1 2) (* 2 2) (/ 5 2))
(if (>= 1 2) (* 2 2) (/ 5 2))
(if (<= 1 2) (* 2 2) (/ 5 2))
